FROM node:12.18.4-alpine

RUN mkdir -p /src

WORKDIR /src

COPY package.json .

RUN yarn install

COPY . /src

RUN yarn build

CMD yarn start
