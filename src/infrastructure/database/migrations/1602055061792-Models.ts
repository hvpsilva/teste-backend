import { MigrationInterface, QueryRunner } from 'typeorm';

export class Models1602055061792 implements MigrationInterface {
  name = 'Models1602055061792';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "vote" ("id" SERIAL NOT NULL, "userId" integer NOT NULL, "vote" integer NOT NULL, "movieIdId" integer, CONSTRAINT "UQ_f5de237a438d298031d11a57c3b" UNIQUE ("userId"), CONSTRAINT "PK_2d5932d46afe39c8176f9d4be72" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE TYPE "movie_genre_enum" AS ENUM('admin', 'user')`);
    await queryRunner.query(
      `CREATE TABLE "movie" ("id" SERIAL NOT NULL, "title" character varying(50) NOT NULL, "description" character varying(250) NOT NULL, "director" character varying(50) NOT NULL, "genre" "movie_genre_enum" NOT NULL, "averageVotes" integer NOT NULL, CONSTRAINT "PK_cb3bb4d61cf764dc035cbedd422" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE TYPE "user_role_enum" AS ENUM('admin', 'user')`);
    await queryRunner.query(
      `CREATE TABLE "user" ("id" SERIAL NOT NULL, "username" character varying(50) NOT NULL, "password" character varying(100) NOT NULL, "role" "user_role_enum" NOT NULL, "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP WITH TIME ZONE, CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE ("username"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "vote" ADD CONSTRAINT "FK_a9a543ec014a37b207098d740fa" FOREIGN KEY ("movieIdId") REFERENCES "movie"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "vote" DROP CONSTRAINT "FK_a9a543ec014a37b207098d740fa"`);
    await queryRunner.query(`DROP TABLE "user"`);
    await queryRunner.query(`DROP TYPE "user_role_enum"`);
    await queryRunner.query(`DROP TABLE "movie"`);
    await queryRunner.query(`DROP TYPE "movie_genre_enum"`);
    await queryRunner.query(`DROP TABLE "vote"`);
  }
}
