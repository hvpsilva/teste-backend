import { getRepository } from 'typeorm';
import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';

import { User } from '~/entities/User';
import { StatusCodes } from 'http-status-codes';
import { Logger } from '~/shared-kernel/Logger';
import { ApiOperationPost, ApiPath } from 'swagger-express-ts';

const SECRET = process.env.JWT_SECRET || '';

@ApiPath({
  path: '/auth',
  name: 'Authentication',
  security: { tokenHeader: [] },
})
export class AuthController {
  @ApiOperationPost({
    path: '/login',
    description: 'Login to API',
    parameters: {
      body: { description: 'Login parameters', required: true, model: 'User' },
    },
    responses: {
      201: { description: 'User created' },
      400: { description: 'Parameters fail' },
    },
  })
  async login(req: Request, res: Response) {
    try {
      Logger.log(`Login to the API`, 'AuthController');
      const userRepository = getRepository(User);

      const { username, password } = req.body;
      if (!(username && password)) {
        res.status(StatusCodes.BAD_REQUEST).send();
      }

      const user = await userRepository.findOne({ where: { username } });
      if (!user) {
        res.status(StatusCodes.UNAUTHORIZED).send();
        return;
      }

      if (!user.checkIfUnencryptedPasswordIsValid(password)) {
        res.status(StatusCodes.UNAUTHORIZED).send();
        return;
      }

      const token = jwt.sign({ userId: user.id, username: user.username }, SECRET, {
        expiresIn: '1h',
      });

      res.send({ token: `Bearer ${token}` });
    } catch (err) {
      Logger.error(`Unable to login. ${err.message}`, err.trace, 'AuthController');
      res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
    }
  }
}
