import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, Unique } from 'typeorm';
import { Movie } from '~/entities/Movie';

@Entity()
@Unique(['userId'])
export class Vote {
  @PrimaryGeneratedColumn()
  id?: number;

  @ManyToOne(() => Movie, (movie) => movie.id)
  movieId?: number;

  @Column()
  userId?: number;

  @Column()
  vote: number;

  constructor(userId: number, vote: number) {
    this.userId = userId;
    this.vote = vote;
  }

  change(vote: number) {
    this.vote = vote;
  }
}
