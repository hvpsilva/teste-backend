import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Genre } from './Genre';
import { Vote } from './Vote';
import { Role } from '~/entities/Role';
import { ApiModel, ApiModelProperty } from 'swagger-express-ts';

@Entity()
@ApiModel({
  description: 'The movie entity.',
  name: 'Movie',
})
export class Movie {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ length: 50 })
  @ApiModelProperty({
    description: 'The title of the movie',
  })
  title: string;

  @Column({ length: 250 })
  @ApiModelProperty({
    description: 'The description of the movie',
  })
  description: string;

  @Column({ length: 50 })
  @ApiModelProperty({
    description: 'The director of the movie',
  })
  director: string;

  @Column({ enum: Role, type: 'enum' })
  @ApiModelProperty({
    description: 'The genre of the movie',
  })
  genre: Genre;

  @OneToMany(() => Vote, (vote) => vote.id)
  votes?: Vote[];

  @Column()
  averageVotes: number;

  constructor(title: string, description: string, director: string, genre: Genre) {
    this.title = title;
    this.description = description;
    this.director = director;
    this.genre = genre;
    this.averageVotes = 0;
  }

  vote(userId: number, vote: number) {
    if (!this.votes) this.votes = [];
    const userVote = this.votes?.find((v) => v.userId === userId);
    if (!userVote) {
      this.votes?.push(new Vote(userId, vote));
    } else {
      userVote.change(vote);
    }

    this.averageVotes = this.votes?.reduce((p, c) => p + c.vote, 0) / this.votes?.length;
  }
}
