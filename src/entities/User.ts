import bcrypt from 'bcryptjs';
import { IsNotEmpty, Length } from 'class-validator';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';

import { Role } from './Role';
import { ApiModel, ApiModelProperty } from 'swagger-express-ts';

@Entity()
@ApiModel({
  description: 'The user of the API. can be a normal user or an admin.',
  name: 'User',
})
@Unique(['username'])
export class User {
  @PrimaryGeneratedColumn()
  id?: number;

  @ApiModelProperty({
    description: "the user's username",
  })
  @Column({ length: 50 })
  @Length(5, 100)
  username: string;

  @ApiModelProperty({
    description: "The user's password",
  })
  @Column({ length: 100 })
  @Length(4, 100)
  password: string;

  @IsNotEmpty()
  @Column({ enum: Role, type: 'enum' })
  @ApiModelProperty({
    description: 'The user`s role. Could be admin ou user',
  })
  role: Role;

  @Column()
  @CreateDateColumn({ type: 'timestamp with time zone' })
  createdAt?: Date;

  @Column()
  @UpdateDateColumn({ type: 'timestamp with time zone' })
  updatedAt?: Date;

  @Column()
  @DeleteDateColumn({ type: 'timestamp with time zone' })
  deletedAt?: Date;

  constructor(username: string, password: string, role: Role) {
    this.username = username;
    this.password = User.hashPassword(password);
    this.role = role;
  }

  private static hashPassword(password: string) {
    return password && bcrypt.hashSync(password, 8);
  }

  checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
    return bcrypt.compareSync(unencryptedPassword, this.password);
  }
}
