import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';

import { User } from '~/entities/User';
import { Role } from '~/entities/Role';
import { StatusCodes } from 'http-status-codes';
import { Logger } from '~/shared-kernel/Logger';

export const checkRole = (roles: Array<Role>) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    const id = res.locals.jwtPayload.userId;

    const userRepository = getRepository(User);

    try {
      const user = await userRepository.findOneOrFail(id);

      if (roles.includes(user.role)) next();
      else res.status(StatusCodes.FORBIDDEN).send();
    } catch (err) {
      Logger.error(`Error in Role check. ${err.message}`, err.trace, 'Authentication');
      res.status(StatusCodes.UNAUTHORIZED).send();
    }
  };
};
