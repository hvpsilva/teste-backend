import { Request, Response, NextFunction } from 'express';
import { StatusCodes } from 'http-status-codes';
import jwt from 'jsonwebtoken';
import { Logger } from '~/shared-kernel/Logger';

const SECRET = process.env.JWT_SECRET || '';

export const checkJwt = (req: Request, res: Response, next: NextFunction) => {
  const token = <string>req.headers['auth'];

  let jwtPayload;

  try {
    jwtPayload = <any>jwt.verify(token?.replace('Bearer ', ''), SECRET);

    res.locals['jwtPayload'] = jwtPayload;
  } catch (err) {
    Logger.error(`Fail to authenticate. ${err.message}`, err.trace, 'Authentication');
    res.status(StatusCodes.UNAUTHORIZED).send();
    return;
  }

  const { userId, username } = jwtPayload;

  const newToken = jwt.sign({ userId, username }, SECRET, { expiresIn: '1h' });

  res.setHeader('token', `Bearer ${newToken}`);

  next();
};
