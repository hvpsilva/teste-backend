import { Router } from 'express';
import swaggerUi from 'swagger-ui-express';

import { AuthController } from '~/controllers/AuthController';
import { UserController } from '~/controllers/UserController';
import { checkRole } from '~/middlewares/checkRole';
import { Role } from '~/entities/Role';
import { checkJwt } from '~/middlewares/checkJwt';
import { MovieController } from '~/controllers/MovieController';

const routes = Router();

// Authentication
const authController = new AuthController();
routes.post('/auth/login', authController.login);

// User
const userController = new UserController();
routes.get('/users' /*, [checkJwt] */, userController.get);
routes.get('/users/:id([0-9]+)', [checkJwt], userController.getById);
routes.post('/users', userController.create);
routes.patch('/users/:id([0-9]+)', [checkJwt], userController.edit);
routes.delete('/users/:id([0-9]+)', [checkJwt], userController.delete);

// Movie
const movieController = new MovieController();
routes.get('/movies', [checkJwt], movieController.get);
routes.get('/movies/:id([0-9]+)', [checkJwt], movieController.getById);
routes.post('/movies', [checkJwt, checkRole([Role.ADMIN])], movieController.create);
routes.patch('/movies/:id([0-9]+)/vote', [checkJwt, checkRole([Role.USER])], movieController.vote);
routes.delete('/movies/:id([0-9]+)', [checkJwt, checkRole([Role.ADMIN])], movieController.delete);

// Swagger
const host = process.env.HOST || 'http://localhost';
const port = process.env.PORT || '3000';
routes.use(
  '/api',
  swaggerUi.serve,
  swaggerUi.setup(undefined, { swaggerOptions: { url: `${host}:${port}/api-docs/swagger.json` } }),
);

export default routes;
